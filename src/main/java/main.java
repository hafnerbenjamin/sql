import com.sun.javafx.collections.MappingChange;
import com.sun.javafx.runtime.SystemProperties;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by Benj on 30.05.2016.
 */
public class main {
    public static void main(String[] args)throws SQLException {
        System.out.println("Penis");
    }












    // Examples von BeniB im unterricht
    static void verbindungAufbauen (){
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila", "root", "root");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("Select * FROM actor")){
/*
            //so wird es als Liste oder Map gespeichert
            Map<Integer, String> idToNameMap = new HashMap<Integer, String>();
            while (resultSet.next()) {
                int id = resultSet.getInt(1); // entweder die spalten nr oder spalten name: "actor_id"
                String name = resultSet.getString("last_name");
                idToNameMap.put(id, name);
            }
            System.out.println(idToNameMap);
*/

            //Oder direkt als Sout
            while (resultSet.next()){
                System.out.println(resultSet.getInt(1) + " " + resultSet.getString(2));
            }

            System.out.println(connection);
        }catch (Exception e){
            System.out.println(e);
        }
    }

    static void uebung1(){
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila", "root", "root");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("Select * FROM actor where actor_id = 1")){

            while (resultSet.next()){
                System.out.println(resultSet.getInt(1) + " " + resultSet.getString(2));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    static void uebung2(){
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila", "root", "root");
            Statement statement = connection.createStatement()){

            String sql = "Select * FROM actor where actor_id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);

            System.out.println("Wechle Id möchstest du sehen?");
            Scanner scanner = new Scanner(System.in);
            int eingabe = scanner.nextInt();
            preparedStatement.setInt(1, eingabe);
            preparedStatement.execute();


            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                System.out.println(resultSet.getInt(1) + " " + resultSet.getString(2));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    static void tryingCon(){
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila", "root", "root");
            Statement statement = connection.createStatement()){

            System.out.println("Wechle Id möchstest du sehen?");
            Scanner scanner = new Scanner(System.in);
            int eingabe = scanner.nextInt();

            ResultSet resultSet = statement.executeQuery("Select * FROM actor where actor_id = " + eingabe);

            while (resultSet.next()){
                System.out.println(resultSet.getInt(1) + " " + resultSet.getString(2));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
